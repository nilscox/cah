import * as player from './player';
import * as game from './game';

export default {
  ...player,
  ...game,
};
