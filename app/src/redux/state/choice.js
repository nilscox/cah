// @flow

export type Choice = {
  id: number,
  text: string,
  keepCapitalization: boolean,
};
