// @flow

import { fetchGame } from './game';
import { createWebSocket, sendWebSocket } from './websocket';

type AfterCallback = { dispatch: Function, getState: Function };

const onPlayerFetched = ({ dispatch, getState }: AfterCallback) => {
  const { player } = getState();

  if (!player)
    return;

  return Promise.resolve()
    .then(() => dispatch(createWebSocket()))
    .then((socket) => {
      // expose the socket in the console
      global.socket = socket;

      dispatch(sendWebSocket({
        action: 'connected',
        nick: player.nick,
      }));
    })
    .then(() => {
      if (player.game)
        return dispatch(fetchGame());
    });
};

export const PLAYER_FETCH = 'PLAYER_FETCH';
export const fetchPlayer = () => ({
  type: PLAYER_FETCH,
  route: '/api/player',
  after: onPlayerFetched,
});

export const PLAYER_LOGIN = 'PLAYER_LOGIN';
export const loginPlayer = (nick: string) => ({
  type: PLAYER_LOGIN,
  route: '/api/player',
  method: 'POST',
  headers: { 'Content-Type': 'application/json' },
  body: JSON.stringify({ nick }),
  after: onPlayerFetched,
});

export const PLAYER_LOGOUT = 'PLAYER_LOGOUT';
export const logoutPlayer = () => ({
  type: PLAYER_LOGOUT,
  route: '/api/player',
  method: 'DELETE',
});

export const PLAYER_UPDATE = 'PLAYER_UPDATE';
export const updatePlayer = (player: {}) => ({
  type: PLAYER_UPDATE,
  route: '/api/player',
  method: 'PUT',
  headers: { 'Content-Type': 'application/json' },
  body: JSON.stringify(player),
});
