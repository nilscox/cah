// @flow

import type { NavigationScreenProp } from 'react-navigation';

export type Navigation = NavigationScreenProp<*>;

export type NavigationProps = {
  navigation: Navigation,
};
