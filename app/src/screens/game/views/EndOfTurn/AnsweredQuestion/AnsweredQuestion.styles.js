import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  wrapper: {
    paddingVertical: 5,
    backgroundColor: '#333',
  },
});
