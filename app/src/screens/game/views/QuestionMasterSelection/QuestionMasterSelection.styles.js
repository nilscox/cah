import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  wrapper: {
    flex: 1,
    alignItems: 'center',
  },
  questionMaster: {
    marginVertical: 35,
  },
});
