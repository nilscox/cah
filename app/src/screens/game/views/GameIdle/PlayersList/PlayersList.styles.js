import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  wrapper: {
    marginVertical: 25,
    flexDirection: 'row',
    justifyContent: 'center',
    flexWrap: 'wrap',
  },
  playerAvatar: {
    marginHorizontal: 5,
  },
});