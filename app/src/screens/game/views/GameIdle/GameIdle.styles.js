import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  wrapper: {
    flex: 1,
    justifyContent: 'flex-start',
    paddingHorizontal: 25,
  },
  waitingText: {
    marginVertical: 50,
    fontSize: 18,
    fontWeight: 'bold',
  },
});