import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  wrapper: {
    flex: 1,
  },
  question: {
    flex: 1,
  },
  choices: {
    flex: 2,
  },
});