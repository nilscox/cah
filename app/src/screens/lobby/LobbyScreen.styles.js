import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  screen: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  gamesListView: {
    flex: 2,
    justifyContent: 'center',
  },
  orView: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  createButtonView: {
    flex: 1,
    paddingTop: 40,
    justifyContent: 'flex-start',
  },
  orText: {
    fontSize: 32,
    paddingHorizontal: 15,
  },
});
