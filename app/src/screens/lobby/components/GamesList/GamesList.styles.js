import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  wrapper: {
    paddingHorizontal: 35,
  },
  joinGame: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: 20,
  },
  joinGameText: {
    fontWeight: 'bold',
    fontSize: 24,
  },
  noGameText: {
    textAlign: 'center',
  },
  reloadIcon: {
    marginHorizontal: 10,
  },
});
