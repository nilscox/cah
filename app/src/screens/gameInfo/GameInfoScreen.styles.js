import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  wrapper: {
    flex: 1,
    paddingHorizontal: 10,
  },

  mainInfo: {
    borderBottomWidth: 1,
    borderBottomColor: '#666',
    paddingBottom: 10,
  },
  mainInfoTitle: {
    color: '#333',
    fontSize: 22,
    fontWeight: 'bold',
    textAlign: 'center',
    marginVertical: 25,
  },
  mainInfoText: {},

  playersList: {
    borderBottomWidth: 1,
    borderBottomColor: '#666',
    paddingBottom: 10,
  },
  playersListTitle: {
    color: '#333',
    fontSize: 22,
    fontWeight: 'bold',
    textAlign: 'center',
    marginVertical: 25,
  },

  gameHistory: {

  },
  gameHistoryTitle: {
    color: '#333',
    fontSize: 22,
    fontWeight: 'bold',
    textAlign: 'center',
    marginVertical: 25,
  },

});
