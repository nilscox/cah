import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  wrapper: {
    padding: 12,
    borderBottomColor: '#ccc',
    borderBottomWidth: 1,
  },
  choice: {
    fontSize: 16,
  },
  selected: {
    backgroundColor: '#cec',
  },
});
