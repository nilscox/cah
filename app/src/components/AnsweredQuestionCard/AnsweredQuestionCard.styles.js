// @flow

import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  wrapper: {
    backgroundColor: '#333',
    height: 50,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  answerText: {
    marginHorizontal: 20,
    color: '#fff',
    fontSize: 16,
    fontWeight: 'bold',
  },
});
