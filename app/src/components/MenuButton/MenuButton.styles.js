import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  trigger: {
    paddingHorizontal: 20,
  },
  triggerText: {
    fontSize: 20,
    color: 'white',
  },
});
