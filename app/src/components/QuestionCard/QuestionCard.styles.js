import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  wrapper: {
    flex: 1,
    backgroundColor: '#333',
  },
  submitted: {
    opacity: 0.8,
  },
});
