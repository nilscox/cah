// @flow

import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  nick: {
    fontSize: 14,
    fontWeight: 'bold',
    textAlign: 'center',
  },
});
