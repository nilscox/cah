// @flow

export const PLAYER_ROUTE = '/api/player';
export const GAME_ROUTE = '/api/game';
export const ANSWER_ROUTE = '/api/answer';

export const API_STATE = {
  UP: 'UP',
  DOWN: 'DOWN',
};

export const WS_STATE = {
  CLOSED: 'CLOSED',
  CREATED: 'CREATED',
  CONNECTED: 'CONNECTED',
};
